       IDENTIFICATION DIVISION. 
       PROGRAM-ID. TRADER.
       AUTHOR. YANISA.
       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT TRADER-FILE ASSIGN TO "trader3.dat"
              ORGANIZATION IS LINE SEQUENTIAL.
      **     SELECT TRADER-REPORT-FILE ASSIGN TO "trader.rpt"
      **       ORGANIZATION IS LINE SEQUENTIAL.
       DATA DIVISION. 
       FILE SECTION.
       FD TRADER-FILE.
       01 TRADER-REC.
           88 END-OF-TRADERS-FILE VALUE HIGH-VALUES .
           05 TRADER-ID   PIC 9(7).
           05 STATE-NUM   PIC 99.
           05 TRADERS     PIC 9(9)V999.
           
        
       WORKING-STORAGE SECTION. 
       
       01  STATE-TRADER-TABLE.
           05 STATE-TRADER-TOTAL   PIC 9(10)V999    OCCURS 77 TIMES .
       01 STATE-IDX    PIC 99.
       01  PAN-STATE-SALES      PIC $$$$,$$$,$$9.
    
       PROCEDURE DIVISION .
       BEGIN.
      **     MOVE ZEROES TO STATE-TRADER-TABLE 
           OPEN INPUT TRADER-FILE 
           READ TRADER-FILE 
              AT END SET END-OF-TRADERS-FILE TO  TRUE 
           END-READ
           PERFORM UNTIL END-OF-TRADERS-FILE 
              ADD TRADERS TO STATE-TRADER-TOTAL(STATE-NUM )
              READ  TRADER-FILE 
                 AT END SET END-OF-TRADERS-FILE TO  TRUE 
             END-READ
           END-PERFORM
           DISPLAY "................." 
           PERFORM  VARYING STATE-IDX FROM 1 BY 1
                       UNTIL STATE-IDX GREATER THAN 77
                 MOVE STATE-TRADER-TOTAL (STATE-IDX ) TO PAN-STATE-SALES
                  
                  DISPLAY "PROVINCE ",STATE-IDX 
                          " PINCOME " PAN-STATE-SALES
                          
           END-PERFORM
      **     CLOSE TRADER-FILE 
           STOP RUN.                 




             